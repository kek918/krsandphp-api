<?php
require_once('init.php');

$method       = trim($_SERVER['REQUEST_METHOD']);
$request_uri  = explode('/', $_SERVER['REQUEST_URI']);
$request_data = [];
parse_str(file_get_contents('php://input'), $request_data);

if(!empty($method) && !empty($request_uri)) {
    try {
        $api = new API($method, $request_uri, $request_data);
        $api->output();
        exit();
    } catch (Exception $e) {
        exit(json_encode('Error: ' . $e->getMessage()));
    }
}
else {
    exit(json_encode("Invalid request"));
}
