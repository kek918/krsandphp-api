<?php
session_start();

define('API_DB_HOST', '127.0.0.1');
define('API_DB_PORT', '3306');
define('API_DB_DB', 'krsandphp');
define('API_DB_USER', 'root');
define('API_DB_PASS', '');

$GLOBALS['config'] = array(
    'version' => array(
        'major' => '0',
        'minor' => '1',
        'patch' => '0'
    )
);

require_once('DB.class.php');
require_once('API.class.php');

date_default_timezone_set('Europe/Oslo');

function escape($str) {
    return trim(htmlspecialchars($str, ENT_QUOTES, 'UTF-8'));
}
