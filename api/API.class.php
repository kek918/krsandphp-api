<?php
class API
{
    // Supported request data types/alias => table name in database
    private $supported_types = [
        'post'     => 'posts',
        'posts'    => 'posts',
        'artikler' => 'posts',
        'artikkel' => 'posts'
    ];

    // Supported request methods
    private $supported_methods = [
        'GET',
        'POST',
        'PUT',
        'DELETE'
    ];

    // The type of data requested - part of the $request_uri array
    // Must be included in $supported_types array above
    private $type = null;

    // The ID of the requested resource/object,
    // for instance when requesting a specific user/article etc
    private $id = null;
    
    // API key/token - part of the $request_uri array
    private $apikey = null;

    // Contains what fields to be updated in database
    // Used for POST and PUT requests.
    private $fields = [];

    // JSON encoded output is saved in this property
    private $output = null;

    /*
    * __construct verifies API request and processes it
    */
    public function __construct($method = null, $request_uri = null, $request_data = null)
    {
        header("Access-Control-Allow-Orgin: *");
        header("Access-Control-Allow-Methods: *");
        header("Content-Type: application/json");
        $this->method  = $method;
        $this->request_uri = $request_uri;
        $this->request_data = $request_data;
        
        // parseRequest will parse $this->request_uri and $this->requst_data 
        // and fill out the properties like $this->apikey, $this->type, $this->action etc.
        $this->parseRequest();

        $this->db = DB::getInstance();

        if(empty($this->method) || !in_array($this->method, $this->supported_methods)) {
            throw new Exception('Invalid method');
        }
        elseif(empty($this->type) || !array_key_exists($this->type, $this->supported_types)) {
            throw new Exception('Invalid data type');
        }
        elseif(empty($this->apikey)) {
            throw new Exception('Invalid API key');
        }
        elseif(!$this->validApiKey($this->apikey)) {
            throw new Exception('Invalid API key');
        }
        elseif(($this->method == 'POST' || $this->method == 'DELETE') && empty($this->id)) {
            throw new Exception('Illegal request. A POST or DELETE request needs an object ID in the URI');
        }
        else {
            // All initial checks passed
            // Continue processing API request
            $this->processRequest();
        }
    }
    
    /*
     * Parse incoming request from $this->request_uri and $this->request_data
     */
    private function parseRequest()
    {
        /*
         * Parsing request_uri:
         * Method /api/type/id/apikey
         * Example: GET /api/post/2913/0923jkfc8u32a31dfxal96ikd
         * Example: PUT /api/post/0923jkfc8u32a31dfxal96ikd
         * See latter PUT example which has no ID because we want to insert a new post.
         *   [0] => 
         *   [1] => krsandphp-api
         *   [2] => api
         *   [3] => post
         *   [4] => 2913
         *   [5] => 0923jkfc8u32a31dfxal96ikd
         */
        $this->type   = $this->request_uri[3];
        $this->id     = count($this->request_uri) === 6 ? $this->request_uri[4] : null;
        $this->apikey = $this->request_uri[count($this->request_uri)-1];    
        
        /*
         * Parsing request_data:
         * GET and DELETE requests won't (or shouldn't) have any request_data,
         * only ID which is already supplied in the URI.
         */
        if($this->method == 'POST' || $this->method == 'PUT') {
            if($this->type == 'post') {
                $this->fields['title'] = $this->request_data['title'];
                $this->fields['author'] = $this->request_data['author'];
                $this->fields['content'] = $this->request_data['content'];
            }
            elseif($this->type == 'someOtherType') {
                $this->fields['something'] = $this->request_data['something'];
                $this->fields['something2'] = $this->request_data['something2'];
                $this->fields['something3'] = $this->request_data['something3'];
            }
        }
    }
    
    /*
     * Process request
     */
    private function processRequest()
    {
        /*
         * Processing the request and running the backend calls to retrieve the data
         */
        
        $query = "";
        $query_params = [];
        $table = $this->supported_types[$this->type];
        
        if($this->method == 'GET') {
            $query = "SELECT * FROM $table";
        }
        
        elseif($this->method == 'POST') {
            $query = "UPDATE $table SET ";
            foreach($this->fields as $field => $new_val) {
                $query .= "$field = ?,";
                $query_params[] = $new_val;
            }
            $query = rtrim($query, ',');
        }
        
        elseif($this->method == 'PUT') {
            $query = "INSERT INTO $table (";
            foreach($this->fields as $field => $val) {
                $query .= "$field,";
            }
            $query = rtrim($query, ',');
            $query .= ') VALUES (';
            foreach($this->fields as $field => $val) {
                $query .= "?,";
                $query_params[] = $val;
            }
            $query = rtrim($query, ',');
            $query .= ");";
        }
        
        elseif($this->method == 'DELETE') {
            $query = "DELETE FROM $table";
        }
        
        if(!empty($this->id)) {
            $query .= " WHERE id = ?";
            $query_params[] = $this->id;
        }
        
        if($this->method == 'GET' && empty($this->id)) {
            $query .= " ORDER BY id DESC";
        }
        
        $this->db->query($query, $query_params);
        if(!$this->db->error()) {
            if($this->db->count() > 0) {
                $this->output = $this->db->results();
            }
            else {
                $this->output = ["Query OK - 0 rows affected"];
            }
        }
        else {
            throw new Exception('Database query failed');
        }
        
        
    }

    private function validApiKey($apikey)
    {
        $this->db->query("SELECT id FROM apikeys WHERE apikey = ?", array($apikey));
        if($this->db->count() == 1) {
            return true;
        }
        return false;
    }

    public function output()
    {
        if(count($this->output) > 1) {
            echo json_encode($this->output);
        }
        else {
            echo json_encode($this->output[0]);
        }
    }

}
