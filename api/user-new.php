<?php
/*
* $_POST contains input fields from signup page.
* Should contain: username, email, password1, password2
*/
if(isset($_POST)) {
    $output = [];
    try {
        $username  = trim($_POST['username']);
        $email     = trim($_POST['email']);
        $password1 = $_POST['password1'];
        $password2 = $_POST['password2'];

        if(empty($username) || empty($email) || empty($password1) || empty($password2)) {
            throw new Exception('One or more input fields are empty.');
        }
        elseif(strlen($username) < 3 || strlen($username) > 20 || !ctype_alnum($username)) {
            throw new Exception('Username must be between 3 and 20 characters and only contain alphanumeric letters (a-Z and 0-9).');
        }
        elseif($password1 !== $password2) {
            throw new Exception('The password fields doesnt match.');
        }
        elseif(strlen($password1) < 8) {
            throw new Exception('Password must be at least 8 characters.');
        }
        elseif(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new Exception('Email address is invalid.');
        }
        else {
            $fields = [
                'action'          => 'insert',
                'type'            => 'user',
                'field_username'  => $username,
                'field_email'     => $email,
                'field_password'  => $password,
                'apikey'          => 'a2jm3a89464as46sia2z24a24kj047'
            ];

            $post_fields = '';
            foreach($fields as $key => $val) {
                $post_fields .= $key.'='.$val.'&';
            }
            $post_fields = rtrim($post_fields, '&');

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "http://localhost:1337/fdm/api");
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
            // $output = curl_exec($ch);
            $output['result'] = curl_exec($ch);
            curl_close($ch);
        }

    }
    catch (Exception $e) {
        $output['result'] = $e->getMessage();
        $output['error'] = true;
    }

    echo(json_encode($output));
}
