# Presentasjon om RESTful API i PHP

- /index.php er selve presentasjonen med slides

|

- /apiclient.php er klient eksempler for å benytte seg av APIet (litt rotete kode... men det viser eksempel i cURL, file_get_contents samt jQuery sin $.ajax, altså XmlHttpRequest)

| 

- /api/API.php er "API serveren" som tar i mot requests og lager en API instans

|

- /api/API.class.php er klassen/metodene som brukes i API serveren

|

- /api/DB.class.php er for håndtering av database queries med PDO

|

Resten er div bootstrap/jquery/misc osv.

|

## Todo:

Lage en komplett klient i PHP i egen klasse.
For eksmpel en cURL klient som støtter alle metodene slik at man kan bruke det i en app på følgende vis:
```
require('APIcURLClient.class.php');

$api = new APIcURL();
$api->setMethod('PUT');
$data = [
    'title'=>'Hello world',
    'author'=>'Test Testosteron',
    'content'=>'Lorem ipsum dolor sit amet...'
];
$api->setData(json_encode($data));
$api->sendRequest();
if(!$api->error()) {
    echo json_encode($api->output());
}
else {
    die(json_encode("Error"));
}
```
for eksempel...