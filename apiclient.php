<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>KristiansandPHP API Client</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="all"  href="css/main.css">
    <link rel="stylesheet" type="text/css" media="all" href="css/whhg.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
</head>

<body>
    
    
    <div class="page-header text-center">
        <h1>API Client</h1>
        <h2>KristiansandPHP v/ Kim Eirik</h2>
    </div>

    <div id="presentation" style="margin-top:120px;">

        

    <?php
    
    if(isset($_GET['post'])) {
        // escape output ..
        $post = json_decode(file_get_contents('http://localhost/krsandphp-api/api/artikkel/'.$_GET['post'].'/0923jkfc8u32a31dfxal96ikd'));
        echo '<div class="panel panel-default">';
        echo '<div class="panel-heading"><h1>'.$post->title.'</h1></div>';
        echo '<div class="panel-body">';
        echo '<p>Author: '.$post->author.'</p>';
        echo '<hr>';
        echo '<p>'.$post->content.'</p>';
        echo '</div></div>';
        echo '<a href="apiclient.php"><button class="btn btn-primary">Go back</button></a> &nbsp; ';
        echo '<a href="apiclient.php?edit-post='.$post->id.'"><button class="btn btn-warning">Edit</button></a> &nbsp; ';
        echo '<a href="apiclient.php?delete-post='.$post->id.'"><button class="btn btn-danger">Delete post</button></a>';
    }
    
    elseif(isset($_GET['edit-post'])) {
        // escape output ..
        $post = json_decode(file_get_contents('http://localhost/krsandphp-api/api/artikkel/'.$_GET['edit-post'].'/0923jkfc8u32a31dfxal96ikd'));
        echo '<form action="#" id="editpost">';
        echo '<div class="panel panel-default">';
        echo '<div class="panel-heading"><input class="form-control" name="title" value="'.$post->title.'" placeholder="Title..."></div>';
        echo '<div class="panel-body">';
        echo '<div><input class="form-control" name="author" value="'.$post->author.'" placeholder="Author..."></div>';
        echo '<hr>';
        echo '<div><textarea name="content" class="form-control" style="height:200px;" placeholder="Content...">'.$post->content.'</textarea></div>';
        echo '</div></div>';
        echo '<div><input type="submit" class="btn btn-primary" value="Submit"></div>';
        echo '</form><br>';
        echo '<a href="apiclient.php"><button class="btn btn-default">Cancel</button></a><br><br>';
        echo '<div id="editpost-status"></div>';
        ?>
<script>
$(document).ready(function(){
    $('form#editpost').on('submit', function(e) {
        e.preventDefault();
        $.ajax({
            url: 'api/post/<?php echo $_GET['edit-post']; ?>/0923jkfc8u32a31dfxal96ikd',
            data: $(this).serialize(),
            type: 'POST',
            dataType: 'JSON',
            success: function(data) {
                $('#editpost-status').html(JSON.stringify(data));
            }
        });
    });
})
</script>
<?php
    }
    
    elseif(isset($_GET['delete-post'])) {
        echo '<a href="apiclient.php"><button class="btn btn-primary">Cancel</button></a>';
        echo '<a href="apiclient.php?edit-post='.$post->id.'"><button class="btn btn-warning">Edit</button></a> &nbsp; ';
        echo '<a href="apiclient.php?delete-post='.$post->id.'"><button class="btn btn-danger">Delete post</button></a>';
    }
    
    elseif(isset($_GET['delete-post'])) {
        // escape output etc...
        $opts = array(
            'http' => array(
                'method'  => 'DELETE',
                'header'  => 'Content-type: application/json'
            )
        );
        $context  = stream_context_create($opts);
        $result = file_get_contents('http://localhost/krsandphp-api/api/post/'.$_GET['delete-post'].'/0923jkfc8u32a31dfxal96ikd', false, $context);
        
        echo $result;
        
        echo '<br><br>';
        echo '<a href="apiclient.php"><button class="btn btn-primary">Go back</button></a> &nbsp; ';
    }
    
    else {
    
    ?>
    

        
        <div class="panel panel-default">
            <div class="panel-heading">Get post</div>
            <div class="panel-body" id="get-api-post-2913-panel">
                <button id="get-api-post-2913" class="btn btn-primary">GET api/post/2913/0923jkfc8u32a31dfxal96ikd</button>
            </div>
        </div>
        
        <hr>
        
        <div class="panel panel-default">
            <div class="panel-heading">Edit post</div>
            <div class="panel-body" id="post-api-post-2913-panel">
                <form action="#" id="post-api-post-2913" method="post">
                    <div><input class="form-control" name="title" id="title" placeholder="Title..."></div><br>
                    <div><input class="form-control" name="author" id="author" placeholder="Author..."></div><br>
                    <div><textarea class="form-control" name="content" id="content" placeholder="Content..." style="height:200px;"></textarea></div><br>
                    <div><input type="submit" value="POST api/post/2913/0923jkfc8u32a31dfxal96ikd" class="btn btn-primary"></div><br>
                </form>
            </div>
        </div>

        <hr>

        <div class="panel panel-default">
            <div class="panel-heading">Add post</div>
            <div class="panel-body" id="put-api-post-panel">
                <form action="#" id="put-api-post" method="post">
                    <div><input class="form-control" name="title" placeholder="Title..."></div><br>
                    <div><input class="form-control" name="author" placeholder="Author..."></div><br>
                    <div><textarea class="form-control" name="content" placeholder="Content..." style="height:200px;"></textarea></div><br>
                    <div><input type="submit" value="PUT api/post/0923jkfc8u32a31dfxal96ikd" class="btn btn-primary"></div><br>
                </form>
            </div>
        </div>
        
        <hr>
        
        <div class="panel panel-default">
            <div class="panel-heading">List posts</div>
            <div class="panel-body" id="list-api-post-panel">
                <?php
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "http://localhost/krsandphp-api/api/posts/0923jkfc8u32a31dfxal96ikd");
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                $output = curl_exec($ch);
                $posts = json_decode($output);

                if(!empty($posts)) {
                    foreach($posts as $post) {
                        echo '<div class="panel panel-default">';
                        echo '<a href="apiclient.php?post='.htmlspecialchars($post->id, ENT_QUOTES, 'UTF-8').'">';
                        echo '<div class="panel-body panel-body-custom-hover">';
                        echo '<h3>'.htmlspecialchars($post->title, ENT_QUOTES, 'UTF-8').'</h3>';
                        echo '<p>Author: '.htmlspecialchars($post->author, ENT_QUOTES, 'UTF-8').'</p>';
                        echo '<p>'.substr(htmlspecialchars($post->content, ENT_QUOTES, 'UTF-8'), 0, 30).'...</p>';
                        echo '<p>&raquo; Read more</p></a>';
                        echo '</div></div>';
                    }
                }
                else {
                    echo 'No posts found in database.';
                }
                ?>
            </div>
        </div>
        
        
        
        
        
    </div> <!-- end presentation div -->

    
    
    
<script>

$(document).ready(function(){
    
    
    // GET api/post/2913
    $('#get-api-post-2913').on('click', function(){
        console.log("Starting XmlHttpRequest");
        $.ajax({url:"api/post/2913/0923jkfc8u32a31dfxal96ikd", type:'GET', dataType:'JSON',
            success: function(data) {
                $('#title').val(data["title"])
                $('#author').val(data["author"])
                $('#content').val(data["content"])
            }
        });
    });
    
    
    // POST api/post/2913 
    $('form#post-api-post-2913').on('submit', function(e) {
        e.preventDefault();
        $.ajax({
            url: 'api/post/2913/0923jkfc8u32a31dfxal96ikd',
            data: $(this).serialize(),
            type: 'POST',
            dataType: 'JSON',
            success: function(data) {
                $('#post-api-post-2913-panel').append(JSON.stringify(data));
            }
        });
    });
    
    
    // PUT api/post/
    $('form#put-api-post').on('submit', function(e) {
        e.preventDefault();
        $.ajax({
            url: 'api/post/0923jkfc8u32a31dfxal96ikd',
            data: $(this).serialize(),
            type: 'PUT',
            dataType: 'JSON',
            success: function(data) {
                $('#put-api-post-panel').append(JSON.stringify(data));
            }
        });
    });
    
    
    
    
})
        
</script>

<?php
    }
?>
    
</body>

</html>    