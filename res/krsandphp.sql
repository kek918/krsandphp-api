-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 09, 2016 at 11:23 PM
-- Server version: 10.0.17-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `krsandphp`
--

-- --------------------------------------------------------

--
-- Table structure for table `apikeys`
--

CREATE TABLE `apikeys` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `apikey` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `access` tinytext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `apikeys`
--

INSERT INTO `apikeys` (`id`, `userid`, `apikey`, `access`) VALUES
(1, 1, '0923jkfc8u32a31dfxal96ikd', '{"get":"yes","post":"yes","put":"yes","delete":"yes"}');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `author` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `author`, `content`) VALUES
(2912, 'Hello World', 'Kim Eirik Kvassheim', 'Morbi erat justo, vulputate non bibendum non, lacinia nec mauris. Nulla pulvinar gravida dolor, sit amet posuere lacus. Ut ultricies ipsum a velit gravida, id ornare lectus suscipit. Curabitur tristique ac magna eu tempor. Donec dictum suscipit laoreet. Sed vel dapibus leo. Vestibulum ut nunc ut sem hendrerit dapibus in et ligula. Vestibulum auctor eros at erat dictum aliquet. Cras leo quam, molestie at nulla tincidunt, egestas tincidunt neque. Sed sodales bibendum dolor iaculis tempor. Ut vel nulla in elit euismod aliquet. Maecenas malesuada convallis dictum. '),
(2913, 'KristiansandPHP Introduksjon til API', 'Kim Eirik Kvassheim', 'KristiansandPHP Introduksjon til APIKristiansandPHP Introduksjon til APIKristiansandPHP Introduksjon til APIKristiansandPHP Introduksjon til APIKristiansandPHP Introduksjon til API'),
(2914, 'Testartikkel - testing', 'Ola Nordmann', ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam blandit eleifend dui, vitae dignissim nisl maximus quis. Sed dignissim mattis vehicula. Integer laoreet dui mollis, dignissim nibh vel, malesuada mi. Fusce id placerat libero. Nulla ac sapien imperdiet, dignissim magna vitae, suscipit diam. Sed viverra justo a pretium semper. Proin quis scelerisque sapien. In sit amet iaculis orci. Quisque accumsan mauris quis tortor faucibus, ac varius metus dictum. Phasellus porta imperdiet egestas. Vivamus dignissim posuere lacinia. Nulla imperdiet arcu eget ipsum interdum efficitur. Suspendisse hendrerit in eros id laoreet. Nunc vitae lobortis ante, eget viverra augue. Sed eget tortor eleifend, varius odio ac, suscipit massa. Praesent at turpis libero.\r\n\r\nPraesent interdum ultricies quam et ornare. Praesent aliquet porta tellus, et vestibulum massa imperdiet ut. Nunc sollicitudin elit in neque mattis, id imperdiet sapien volutpat. Nullam at turpis faucibus, volutpat nibh placerat, pulvinar orci. Vestibulum odio nibh, faucibus et purus nec, vehicula lacinia elit. Morbi pretium augue a dolor finibus, bibendum placerat leo egestas. In dapibus elementum mi, eget egestas eros dictum viverra. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum et eros nec urna consequat viverra. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent porta vel tellus eu porttitor. Vivamus purus lorem, varius et orci sed, accumsan accumsan nibh. ');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `apikeys`
--
ALTER TABLE `apikeys`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `userid` (`userid`),
  ADD UNIQUE KEY `key` (`apikey`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `title` (`title`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `apikeys`
--
ALTER TABLE `apikeys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2915;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
