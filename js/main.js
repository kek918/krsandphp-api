$(function(){

    function sleepFor( sleepDuration ){
        var now = new Date().getTime();
        while(new Date().getTime() < now + sleepDuration){ /* do nothing */ }
    }

    /*
    * Makes it possible to click on an alert to hide it
    */
    $('.alert').on('click', function() {
        $(this).fadeOut(1000);
    });


    /*
    * Initiate carousel slider
    */
    $('#carousel-kphp').carousel({
        interval: false,
        keyboard: true
    });

    /*
    * Event handler for changing presentation slides
    */
    var current_slide = 0;
    var total_slides = $('.slide').size();
    var slide_length_progressbar = 100 / (total_slides - 1);
   

    $('body').keydown(function(e){
        if(e.keyCode == 39) {
            /*
             * Arrow key RIGHT
             */
            if(current_slide == (total_slides-1)) {
//                $('#slide-'+(current_slide)).css('display','none').fadeOut(300);
//                $('#slide-0').css('display','block').fadeIn(300);
                $('#slide-'+current_slide).hide('slow');
                $('#slide-0').show('slow');
                current_slide = 0;
            }
            else {
               $('#slide-'+current_slide).hide('slow');
               $('#slide-'+(current_slide+1)).show('slow');
                current_slide++;
            }
        }
        else if(e.keyCode == 37) {
            /*
             * Arrow key LEFT
             */
            if(current_slide == 0) {
//                $('#slide-'+current_slide).css('display','none');
//                $('#slide-'+total_slides).css('display','block');
                $('#slide-'+current_slide).hide('slow');
                $('#slide-'+(total_slides-1)).show('slow');
                current_slide = (total_slides-1);
            }
            else {
                $('#slide-'+current_slide).hide('slow');
                $('#slide-'+(current_slide-1)).show('slow');
                current_slide--;
            } 
        }
        
        $('#progress-bar').width(current_slide * slide_length_progressbar + '%');
        
                        /*
                var current_progress_bar = Math.round(parseFloat($('#progress-bar').width() / $('#progress-bar').parent().width() * 100));
                var new_progress_bar = current_progress_bar + 10;
                $('#progress-bar').width(new_progress_bar+'%');
                */
               
//        if(current_slide < 1 || current_slide > 10) {
//            current_slide = 0;
//        }
    
    });



    /*
    * Event handler for signup form Fresh Design Minds
    */
    $('form#signup-fdm-form').on('submit', function(event) {
        event.preventDefault();
        $('#request-status').removeClass().addClass('alert').addClass('alert-info');
        $('#request-status').html('<img src="res/loading.gif"> &nbsp; Please wait...').show();
        // $('#loading').show();
        var data = $(this).serialize();



        // if($('#signup-fdm-pw1').val() != $('#signup-fdm-pw2').val() || $('#signup-fdm-pw1').val() == '') {
        //     $('#signup-fdm-pw1').addClass('invalid').delay(1000).queue(function(){
        //         $('#signup-fdm-pw1').removeClass('invalid').dequeue();
        //     });
        //     $('#signup-fdm-pw2').addClass('invalid').delay(1000).queue(function(){
        //         $('#signup-fdm-pw2').removeClass('invalid').dequeue();
        //     });
        //     $('.signup-form-error').css('display', 'block').html('Password mismatch or invalid password.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
        // }
        // else {

        $.ajax({
            type: "POST",
            url: "api/user-new.php",
            data: data,
            dataType: "json",
            success: function(data){
                if(data.error) {
                    $('#request-status').removeClass('alert-info').addClass('alert-danger');
                }
                else {
                    $('#request-status').removeClass('alert-info').addClass('alert-success');
                }
                $('#request-status').html(data.result);
            },
            error: function(xhr, status) {
                $('#request-status').removeClass('alert-info').addClass('alert-danger');
                $('#request-status').html('Error: ' + status);
            }
        });

        // }
    });





});
