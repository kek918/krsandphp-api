<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>KristiansandPHP</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="all"  href="css/main.css">
    <link rel="stylesheet" type="text/css" media="all" href="css/whhg.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/TimelineMax.min.js"></script>
    <script src="js/TweenLite.min.js"></script>
</head>

<body>


    <div class="page-header text-center">
        <h1>Application Programming Interface</h1>
        <h2>KristiansandPHP v/ Kim Eirik</h2>
    </div>

    <div class="progress">
        <div class="progress-bar" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width:0%;" id="progress-bar"></div>
    </div>



    <div id="presentation">

        <div class="slide active" id="slide-0">
            <h3>Kort introduksjon til API - 30 min</h3>
            <ul>
                <li>Kjapt om meg</li>
                <li>REST/CRUD/HTTP</li>
                <li>Eksempel API klient i PHP</li>
                <li>Eksempel API server i PHP</li>
                <li>Kilder/lesestoff</li>
            </ul>
        </div>

        <div class="slide" id="slide-1">
            <h3>Kjapt om meg</h3>
            <ul>
                <li>Kim Eirik Kvassheim, 24 år, bor i Grimstad med samboer og sønn på 4 år</li>
            </ul>
            <div id="picturebox">
                <img src="res/jjuc.png" alt="JJUC" class="img-jjuc">
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                <img src="res/teknix.png" alt="Teknix" class="img-teknix">
                <br><br>
                <img src="res/junior.jpg">
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                <img src="res/travel-streetview.jpg" class="clipimg">
                <br><br><br><br><br><br><br><br>
            </div>
        </div>
        
        <div class="slide" id="slide-2">
            <h3>REST/CRUD/HTTP</h3>
            <ul>
                <li>HTTP er basert på REST prinsippet (Representational State Transfer)<br>
                    Klient → Server tenker ut en vettug respons → Klient</li>
                <li>Metoder definert i HTTP: GET, POST, PUT, DELETE, PATCH, HEAD, OPTIONS, TRACE, CONNECT</li>
                <li><b>Kun</b> GET/POST er støttet i HTML. For å bruke PUT/DELETE i en standard HTML form må man enten sende via XmlHttpRequest eller komme rundt begrensningen med et custom input felt (feks hidden input "_method").</li>
                <li>CRUD = Create[PUT], Read[GET], Update[POST], Delete[DELETE]</li>
                <li>Hvorfor bruke API? For å holde backend/frontend-kode adskilt. Enklere å vedlikeholde og å legge til nye klienter/apper etc. <b>*Ser på deg legacy kode*</b></li>
            </ul>
        </div>
        
        <div class="slide" id="slide-3">
            <h3>Eksempel API klient</h3>
            <ul>
                <li>
                    Batch - Windows Management Instrumentation CLI for Windows:<br>
<pre>
wmic csproduct get name
wmic computersystem get
</pre>
                </li>
                <li>
                    PHP - Stripe
<pre># php
\Stripe\Stripe::setApiKey("sk_test_BQokikJOvBiI2HlWgH4olfQ2");

\Stripe\Charge::create(array(
  "amount" => 400,
  "currency" => "usd",
  "source" => "tok_17xzcm2eZvKYlo2CUeeZaXWE", // obtained with Stripe.js,
  "metadata" => array("order_id" => "6735")
));
</pre>
                </li>
                <li>
                    Python - Google Translate (<a href="https://cloud.google.com/translate/v2/pricing">$20 per 1 M characters of text</a>)
<pre>
from googleapiclient.discovery import build

def main():

  # Build a service object for interacting with the API. Visit
  # the Google APIs Console <http://code.google.com/apis/console>
  # to get an API key for your own application.
  service = build('translate', 'v2',
            developerKey='AIzaSyDRRpR3GS1F1_jKNNM9HCNd2wJQyPG3oN0')
  print(service.translations().list(
      source='en',
      target='fr',
      q=['flower', 'car']
    ).execute())

if __name__ == '__main__':
  main()
</pre>
                </li>
                <li>
                    HTTP - Hent brukerdetaljer fra StackExchange (StackOverflow)
<pre>
GET api.stackexchange.com/2.2/users/3492590?order=desc&sort=reputation&site=stackoverflow
</pre>
                    <button id="get-stackoverflow" class="btn btn-primary">GET https://api.stackexchange.com/2.2/users/3492590?order=desc&sort=reputation&site=stackoverflow</button><br>
                    <pre id="get-stackoverflow-status"></pre><br><br>
<script>
document.addEventListener('DOMContentLoaded', function() {
    document.getElementById("get-stackoverflow").addEventListener("click", function(e){
        e.preventDefault();
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function(){
            if(xhttp.readyState == 4 && xhttp.status == 200) {
                document.getElementById("get-stackoverflow-status").innerHTML = xhttp.responseText;
            }
        };
        var apiurl = 'https://api.stackexchange.com/2.2/users/3492590?order=desc&sort=reputation&site=stackoverflow';
        xhttp.open('GET', apiurl, true);
        xhttp.send();
    });  
});
</script>
                </li>
            </ul>
        </div>
        
        <div class="slide" id="slide-4">
            <h3>Eksempel API server</h3>
            <ul>
                <li>API server venter på innkommende requests og gjør følgende:
                    <ul>
                        <li>Sjekk method (GET/POST etc)</li>
                        <li>Sjekk request (feks GET /bruker/kek)</li>
                        <li>Sjekk authentication (en hash e.l. med riktig rettigheter)</li>
                        <li>Hvis method, request og authentication er ok, gå videre og prosesser request og output i ønsket format</li>
                    </ul>
                </li>
            </ul>
            <pre>
<<!-- -->?<!-- -->php
$method  = $_SERVER['REQUEST_METHOD'];
$request = explode("/", substr(@$_SERVER['PATH_INFO'], 1));
$apikey  = $request[count($request)-1];

if(verify_apikey($apikey)) {
    if($method == "GET") {
        // Hent X fra databasen
    }
    elseif($method == "POST") {
        // Rediger X i databasen
    }
    elseif($method == "PUT") {
        // Legge til X i databasen
    }
    elseif($method == "DELETE") {
        // Slett X fra databasen
    }
    else {
        die(json_encode("Unknown request method"));
    }
}
else {
    die(json_encode("Authentication failed"));
}
?>
            </pre>
        </div>
        
        <div class="slide" id="slide-5">
            <h3>Kilder/lesestoff</h3>
            <ul>
                <li>HTTP Methods: <a href="https://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html">https://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html</a></li>
                <li>Introduksjon til REST: <a href="http://rest.elkstein.org/">http://rest.elkstein.org/</a></li>
                <li>API Directory: <a href="http://www.programmableweb.com/apis/directory">http://www.programmableweb.com/apis/directory</a></li>
                <li><a href="http://programmers.stackexchange.com/questions/114156/why-are-there-are-no-put-and-delete-methods-on-html-forms">http://programmers.stackexchange.com/questions/114156/why-are-there-are-no-put-and-delete-methods-on-html-forms</a></li>
            </ul>

    </div>







</body>
</html>
