<?php
class DB
{
    private static $_instance = null;
    private $_pdo,
            $_query,
            $_error = false,
            $_results,
            $_count = 0;

    private function __construct()
    {
        try
        {
            $dsn_host       = API_DB_HOST;
            $dsn_port       = API_DB_PORT;
            $dsn_password   = API_DB_PASS;
            $dsn_username   = API_DB_USER;
            $dsn_dbname     = API_DB_DB;
            $dsn = "mysql:host=$dsn_host;port=$dsn_port;dbname=$dsn_dbname;charset=utf8";
            $this->_pdo = new PDO($dsn, $dsn_username, $dsn_password);
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public static function getInstance()
    {
        if(!isset(self::$_instance))
        {
            self::$_instance = new DB();
        }
        return self::$_instance;
    }

    public function query($sql, $params = array())
    {
        $this->_error = false;
        if($this->_query = $this->_pdo->prepare($sql))
        {
            $i = 1;
            if(count($params))
            {
                foreach($params as $param)
                {
                    $this->_query->bindValue($i, $param);
                    $i++;
                }
            }
            if($this->_query->execute())
            {
                if($this->_query->columnCount())
                    $this->_results = $this->_query->fetchAll(PDO::FETCH_OBJ);
                $this->_count = $this->_query->rowCount();
            }
            else
            {
                $this->_error = true;
            }
        }
        return $this;
    }

    public function action($action, $table, $where = array())
    {
        if(count($where) == 3)
        {
            $operators = array('=', '>', '<', '>=', '<=', '!=', 'LIKE');
            $field      = $where[0];
            $operator   = $where[1];
            $value      = $where[2];

            if(in_array($operator, $operators))
            {
                if(strpos($action, "LEFT JOIN"))
                {
                    $sql = "{$action} WHERE {$field} {$operator} ?";
                    if(!$this->query($sql, array($value))->error())
                    {
                        return $this;
                    }
                    return $sql;
                }
                else
                {
                    $sql = "{$action} FROM {$table} WHERE {$field} {$operator} ?";
                    if(!$this->query($sql, array($value))->error())
                    {
                        return $this;
                    }
                }
            }
        }
        return false;
    }

    public function get($table, $where)
    {
        return $this->action('SELECT *', $table, $where);
    }

    public function getLeftJoin($table, $fields = '*', $joinTable, $joinField1, $joinField2, $where)
    {
        $action = "SELECT {$fields} FROM {$table} LEFT JOIN {$joinTable} ON {$table}.{$joinField1} = {$joinTable}.{$joinField2}";
        return $this->action($action, $table, $where);
    }

    public function delete($table, $where)
    {
        return $this->action('DELETE', $table, $where);
    }

    public function insert($table, $fields = array())
    {
        if(count($fields))
        {
            $keys = array_keys($fields);
            $values = '';
            $i = 1;

            foreach($fields as $field)
            {
                $values .= '?';
                if($i < count($fields))
                {
                    $values .= ', ';
                }
                $i++;
            }

            $sql = "INSERT INTO {$table} (`" . implode('`, `', $keys) . "`) VALUES ({$values})";

            if(!$this->query($sql, $fields)->error())
            {
                return true;
            }
        }
        return false; // returns false because not enough data was supplied for insert() method
    }

    public function update($table, $id, $idtype, $operator, $fields)
    {
        $set = '';
        $i = 1;

        foreach($fields as $name => $value)
        {
            $set .= "{$name} = ?";
            if($i < count($fields))
            {
                $set .= ', ';
            }
            $i++;
        }

        $sql = "UPDATE {$table} SET {$set} WHERE {$idtype} {$operator} {$id}";

        if(!$this->query($sql, $fields)->error())
        {
            return true;
        }
        return false;

    }

    public function results()
    {
        return $this->_results;
    }

    public function result($i)
    {
        return $this->results()[$i];
    }

    public function first()
    {
        return $this->results()[0];
    }

    public function last()
    {
        $totalRows = $this->count();
        return $this->results()[$totalRows-1];
    }

    public function error()
    {
        return $this->_error;
    }

    public function count()
    {
        return $this->_count;
    }

    public function changeAttributes($args1, $args2)
    {
        $this->_pdo->setAttribute($args1, $args2);
    }

    public function beginTransaction()
    {
        $this->_pdo->beginTransaction();
    }

    public function commit()
    {
        $this->_pdo->commit();
    }

    public function rollBack()
    {
        $this->_pdo->rollBack();
    }
}
